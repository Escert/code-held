# Code-Held.com Source Code

This repository is the source code of the [code-held.com](https://code-held.com) website. Feel free to use this page as a template for your own page. If you do so a credit on your page is very appreciated.

## How to Develop

I provide a [docker-compose](docker-compose.yml) file that boots up jekyll and all its dependecies in the correct version.  
By executing `docker-compose up` you'll make the page available on `localhost:4000`.
This docker-compose file is also used for building in the [.gitlab-ci.yml](.gitlab-ci.yml).

### Update Dependencies

As soon as you update/add any dependency to jekyll you need to run 
```
docker-compose run blog bundle update
```
to install it in your container.