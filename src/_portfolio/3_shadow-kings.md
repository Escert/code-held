---
title: Shadow Kings
image: portfolio/shadow-kings.jpg
links:
 - href: /assets/pdf/shadow_kings.pdf
   text: Download Factsheet
---
<span class="portfolio-section-title">What is it?</span>

As a successor of Goodgame Empire it was planned to target a more casual audience with a similar gameplay. Shadow Kings got released on PC, iOS and Android.

<span class="portfolio-section-title">What did I do?</span>

I took over the project in the last months of its existent as the first backend developer. In this time it was my responsibility to lead a team of 6 backend developers and to fork off the server from its origins in Goodgame Empire.
