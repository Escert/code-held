---
title: Gates of Epica
image: portfolio/gates-of-epica.jpg
links:
 - href: /assets/pdf/GoE_Credits.pdf
   text: Download Credits
---
<span class="portfolio-section-title">What is it?</span>

Gates of Epica was an action RPG developed with the Unreal Engine 4 for iOS and Android. In the game the player fought for loot and glory in more then 600 hand-crafted missions and joined glorious multiplayer boss fights where many players fought a boss for days.

<span class="portfolio-section-title">What did I do?</span>

My responsibility was the development of the backend and game logic. We used Java with Spring Boot, postgreSQL and hibernate.
