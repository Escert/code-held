$(document).ready(function(){
  $('.parallax').parallax();
  $('.fixed-action-btn').floatingActionButton({hoverEnabled: false});
  $('.sidenav').sidenav();
  $('.tooltipped').tooltip();
  $('.slider').slider();
  $('.scrollspy').scrollSpy();
  $('.materialboxed').materialbox();
});
