---
title: Tools
icons:
  - {icon: apple, tooltip: Apple}
  - {icon: bitbucket, tooltip: Bitbucket}
  - {icon: confluence, tooltip: Confluence}
  - {icon: debian, tooltip: Debian}
  - {icon: git, tooltip: Git}
  - {icon: github, tooltip: GitHub}
  - {icon: gitlab, tooltip: Gitlab}
  - {icon: intellij, tooltip: IntelliJ}
  - {icon: linux, tooltip: Linux}
  - {icon: slack, tooltip: Slack}
  - {icon: sourcetree, tooltip: Sourcetree}
  - {icon: ssh, tooltip: ssh}
  - {icon: vim, tooltip: vim}
  - {icon: windows8, tooltip: Windows}
---
