---
title: Technologies
icons:
  - {icon: csharp, tooltip: C#}
  - {icon: css3, tooltip: CSS}
  - {icon: docker, tooltip: Docker}
  - {icon: dot-net, tooltip: .NET}
  - {icon: gradle, tooltip: Gradle}
  - {icon: html5, tooltip: HTML5}
  - {icon: java, tooltip: Java}
  - {icon: mysql, tooltip: MySQL}
  - {icon: nginx, tooltip: nginx}
  - {icon: postgresql, tooltip: PostgreSQL}
  - {icon: redis, tooltip: Redis}
  - {icon: tomcat, tooltip: Tomcat}
---
