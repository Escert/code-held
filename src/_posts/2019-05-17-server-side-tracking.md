---
layout: post
title: That's How Wrong Google Analytics Can Be
image: assets/images/posts/2019/05/statistics.jpg
image-alt: Statistics on a website
tags: [Webdesign, DevOps]
highlight: false
call-to-action: Do you do server side tracking? Get in touch with me and 
credits: Post image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@srd844?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Stephen Dawson"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Stephen Dawson</span></a>
---
This blog is not very old yet. I just started it a month ago and of course I wanted to know if somebody is even reading it. So after a few days I included Google Analytics to get an impression of the visitors of my page and if I even have an audience here. But barely any visit got tracked and since I didn't even filter out my own ip I had the impression that I'm basically the only user of my page. But that impression was wrong, which I noticed when I switched to server side tracking.

{% asset posts/2019/05/google-analytics-visits.png class="responsive-img materialboxed" width="500" data-caption="My visits according to google analytics." alt="My visits according to google analytics" %}

As you can see I barely got more than one user on any day. That can have many reasons. One is, my target audience of this blog are developers and its very likely that every one (including myself) has some kind of ad or script blocker extension running. Another is that I had a cookie consent in place that actually worked and since the page was usable without accepting it and it wasn't even very prominent the few users that don't have any kind of blocker probably didn't accept the cookie consent and as a result weren't tracked.

After a couple of days when I had the first posts in place I started with a little promotion on my [Twitter account](https://twitter.com/MarHeldro) and realised that according to their statistics I should see at least 26 visits on my page, but no one was present in Google Analytics.

{% asset posts/2019/05/twitter-visits.png class="responsive-img materialboxed" width="600" data-caption="Twitter statistics of one of my tweets" alt="Twitter statistics of one of my tweets" %}

So I decided I set up some kind of server side tracking and see where it leads. And the difference is **huge**. On the first day that I tracked I had nearly 200 unique visitors in contrast to 2 in Google Analytics.

{% asset posts/2019/05/server-side-visits.png class="responsive-img materialboxed" width="900" data-caption="My tracking data with server side tracking" alt="My tracking data with server side tracking" %}

Technologically I decided for the open source variant of [matomo](https://matomo.org/) which has such a large feature set that its more then enough for my needs where I mainly want to know which content is read and how my page develops over time.

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Fun fact: I have an increase of 10.000% unique visitors by changing to server side tracking with <a href="https://twitter.com/matomo_org?ref_src=twsrc%5Etfw">@matomo_org</a> instead of <a href="https://twitter.com/googleanalytics?ref_src=twsrc%5Etfw">@googleanalytics</a>. <a href="https://twitter.com/hashtag/adblock?src=hash&amp;ref_src=twsrc%5Etfw">#adblock</a> and co are to widespread nowadays. (to be fair, I only had 2 visitors according to google analytics yesterday 😅) <a href="https://twitter.com/hashtag/hosting?src=hash&amp;ref_src=twsrc%5Etfw">#hosting</a> <a href="https://twitter.com/hashtag/Analytics?src=hash&amp;ref_src=twsrc%5Etfw">#Analytics</a></p>&mdash; Marcus Held (@MarHeldro) <a href="https://twitter.com/MarHeldro/status/1124202790144937984?ref_src=twsrc%5Etfw">May 3, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
