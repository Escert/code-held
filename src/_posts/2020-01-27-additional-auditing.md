---
layout: post
title: Additional auditing with Spring Data JPA and Hibernate
image: assets/images/posts/2020/01/additional-auditing.jpg
image-alt: Pile of papers
tags: [Spring Data, JPA, Hibernate, Auditing]
author: Sascha Boeing
highlight: false
call-to-action: Feel free to try it out yourself.
credits: Post image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@joaosilas?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from João Silas"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">João Silas</span></a>
---
Spring Data provides an easy way of keeping track who creates and modifies a persistent entity as well as when the action happened by annotating properties with @CreatedBy, @CreatedDate, @LastModifiedBy and @LastModifiedDate. The properties are automatically provided by an implementation of the AuditAware and DateTimeProvider interface.

Beside this common auditing information in my current projects some entities require storing auditing-information about crucial state-changes like soft-deletion.

Previously we had to fill the properties ourself.

{% highlight java %}
@Entity
public class SomeEntity {
	@Column
	private String deletedBy;
	@Column
	private OffsetDateTime deletedOn;

	public void markAsDeleted(String currentUser, OffsetDateTime currentDateTime) {
		this.deletedBy = currentUser;
		this.deletedOn = currentDateTime;
	}
}

public class SomeService {
	
	private CurrentUserProvider currentUserProvider;
	private CurrentDateTimeProvider currentDateTimeProvider;

	public void markAsDeleted(SomeEntity someEntity) {
		String currentUser = currentUserProvider.currentUser();
		OffsetDateTime currentDateTime = currentDateTimeProvider.now();

		someEntity.markAsDeleted(currentUser, currentDateTime);
	}
}
{% endhighlight %} 

This is of cause not a big thing but anyway: Why do we have to do it? Why can't we simply use the same 'magic' that is working for the regular auditing properties?

### Basic Idea

So the simplest approach is to use @PreUpdate to fill the additional auditing properties. To avoid updating them on **each** update of our entity we use a transient boolean.

{% highlight java %}
@Entity
@EntityListeners(DeletionListener.class)
public class SomeEntity {
	@Column
	private String deletedBy;
	@Column
	private OffsetDateTime deletedOn;

	public transient boolean markedForDeletion = false;

	public void markAsDeleted() {
		this.markedForDeletion = true;
	}

	public void fillDeletion(String currentUser, OffsetDateTime currentDateTime) {
		this.deletedBy = currentUser;
		this.deletedOn = currentDateTime;
	}
}

public class DeletionListener {
	
	private CurrentUserProvider currentUserProvider;
	private CurrentDateTimeProvider currentDateTimeProvider;

	@PreUpdate
	public void touchForUpdate(Object target) {
		SomeEntity entity = (SomeEntity) target;
		if(entity.markedForDeletion) {
			String currentUser = currentUserProvider.currentUser();
			OffsetDateTime currentDateTime = currentDateTimeProvider.now();

			someEntity.fillDeletion(currentUser, currentDateTime);
		}
	}
}
{% endhighlight %}

This has just one problem: @PreUpdate is only called when something on the entity has been changed and an update is necessary. But since markedForDeletion is transient changing its value does not trigger an update.

### Simple solution

A quite simple solution is to just change 'something' on the entity together with marking it for deletion. To guarantee triggering a PreUpdate while not having additional unnecessary persistent properties we can either change the deletedBy or deletedOn property to some temporary value. The @PreUpdate method will override these values with correct values eventually.

{% highlight java %}
@Entity
@EntityListeners(DeletionListener.class)
public class SomeEntity {
	@Column
	private String deletedBy;
	@Column
	private OffsetDateTime deletedOn;

	public transient boolean markedForDeletion = false;

	public void markAsDeleted() {
		deletedOn = LocalDate.of(1337, 1, 1).atTime(OffsetTime.now());
		this.markedForDeletion = true;
	}
}
{% endhighlight %}

### Complex solution

A more complex solution is telling Hibernate that the entity has been updated without unnecessarily changing properties. Hibernate facilitates that by supporting a CustomEntityDirtinessStrategy. But this would require us to check the full entity including Collections and other complex property types. We definitely don't want!
Instead, we can create a CompositeUserType for our additional auditing properties. CompositeUserTypes are checked for dirtiness by calling an equals-method, comparing their current instance with the originally loaded snapshot.

{% highlight java %}
@Entity
@TypeDef(name = "Deletable", typeClass = DeletableType.class, defaultForType = Deletable.class)
@EntityListeners(DeletionListener.class)
public class SomeEntity {
	@Columns(columns = {@Column(name = "deleted_by"), @Column(name = "deleted_on")})
	private Deletable deletable = new Deletable();

	public void markAsDeleted() {
		deletable.markAsDeleted();
	}
}

public class Deletable {
	private UserName deletedBy;
	private OffsetDateTime deletedOn;
	private transient boolean markAsDeleted = false;

	public void markAsDeleted() {
		markAsDeleted = true;
	}

	public void fillDeletion(String currentUser, OffsetDateTime currentDateTime) {
		this.deletedBy = currentUser;
		this.deletedOn = currentDateTime;
	}
}

public class DeletableType implements CompositeUserType {
	@Override
	public boolean equals(Object x, Object y) {
		if (x == y) {
			return true;
		}
		if (y == null || x.getClass() != y.getClass()) {
			return false;
		}
		Deletable xDeletable = (Deletable) x;
		Deletable yDeletable = (Deletable) y;
		return xDeletable.markAsDeleted == yComponent.markAsDeleted &&
				Objects.equals(xDeletable.deletedBy, yDeletable.deletedBy) &&
				Objects.equals(xDeletable.deletedOn, yDeletable.deletedOn);
	}

	@Override
	public int hashCode(Object x) {
		Deletable deletable = (Deletable) x;
		return Objects.hash(deletable.deletedBy, deletable.deletedOn, deletable.markAsDeleted);
	}
}
{% endhighlight %}

### Summary

So both solutions have their drawbacks. On the one hand it is very technical and not domain-driven to have these temporary values. On the other it's a tad on the over-engineering side to provide an CompositeUserType for something as simple as 2 auditing-fields.
Anyways, there are examples enough for those auditing-requirements, examples include who/when sent some email, downloaded a file or triggered that process.

So which solution do you prefer?
Personally I would choose the complex one as it is cleaner.

