---
layout: post
title: Never Design A Class That Knows How It's Used
image: assets/images/posts/2019/06/class-knowledge-title.jpg
image-alt: Coffee in front of a laptop
tags: [Java, Clean Code]
highlight: true
call-to-action: Did you run into the same problem? Tell me and 
credits: Post Image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@amrvle?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Gustas Brazaitis"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Gustas Brazaitis</span></a>
---
When you design a class you should never design it in a way that the class itself knows how it is used from the outside. Breaking this principle will make it difficult for other developers providing other implementations. I recently stumbled upon an implementation of different entities where the implementation had to provide a unique id for itself. Why this is causing problems and how you better design such a situation I'll explain in this post.

Let's introduce an example of the problem that I want to talk about. There is an interface called `Entity` that specifies a method {% ihighlight java %}getId(){% endihighlight %} that needs to provide a unique id.

{% highlight java %}
public interface Entity {
	
  /**
   * @return an id that is unique over all {@link Entity} implementations in the application. 
   */
  String getId();
}
{% endhighlight %}

We also have an implementation called `House` of this interface:

{% highlight java %}
public class House implements Entity {
	
  private Address address;
  
  @Override
  public String getId() {
    return address.toString();
  }
} 
{% endhighlight %}

Another implementation might be a `PostCard`:

{% highlight java %}
public class PostCard implements Entity {
	
  private Address from;
  
  private Address to;
  
  @Override
  public String getId(){
    return from.toString() + to.toString();
  }  
}
{% endhighlight %}

You can already imagine the problem that's present here. We can easily have a violation of the contract of an `Entity`. As soon as one of the addresses of the `PostCard` is empty we can end up with a `House` that returns the same id and this is against the contract of an `Entity`.
So without knowing all implementations of the `Entity` you can never be sure if you actually provide a unique Id or not. This problem gets worse, the more implementations you have.

Another problem is that you are never able to write a unit test that can actually test the requirement that the id is unique over the whole application.

The underlying problem here is that we put the responsibility to provide unique ids to the wrong layer in our application. The only instance that can control, and ensure, that we have unique ids is the class that creates the entities in our application. And if we don't have such a class yet than we need to provide a factory for that.

{% highlight java %}
public class House implements Entity {
  private String id;
  private Address address;
  
  House(String id, Address address) {
    this.id = id;
    this.address = address;
  }
  
  (...)
}


public class EntityFactory {
  
  private IdGenerator idGenerator;
  
  public House createHouse(Address address) {
    return new House(idGenerator.next(), address);
  }

}
{% endhighlight %}

We can easily test that {% ihighlight java %}idGenerator.next(){% endihighlight %} always provides a unique id. And with restricted visibility of the constructor we can ensure that you can't create a `House` otherwise.
