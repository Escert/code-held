---
layout: post
title: Welcome to my Blog
image: assets/images/posts/2019/04/welcome.jpg
image-alt: Colorful welcome headline
tags: []
call-to-action: Did this article help you? I'd love to hear from you.
credits: Post Image by 
         <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@bel2000a?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Belinda Fewings"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Belinda Fewings</span></a>
---
Previously I only used to have a simple portfolio website at this place. But in my day to day work I figured that I actually get in touch with a lot of awesome technology and daily I discover something interesting that is worth writing about to help others in the same situation. This is what this blog will be about. I'll share my knowledge, give hints on problems related to programming and operating a modern software and tell my opinion on programming styles and recent debates.

I don't know where this blog will lead me to, but it will be my place to play with.

By the way, I did a blog in the past already in german during my study. The blog was called _blogasart.de_ and actually quite popular since I took the time to go over the hardest content of my study. Maybe I can find these entries and translate it for publishing at this page.

But that's enough for now. Stay tuned for new content and subscribe if you're interested.
