---
layout: post
title: Build With Jekylls' Docker Image and Gitlab CI
image: assets/images/posts/2019/04/gear.jpg
image-alt: Brown Gear
tags: [Jekyll, Docker, Gitlab, Continuous Integration]
highlight: true
call-to-action: Did this article help you? I'd love to hear from you.
credits: Post Image by 
         <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@mlightbody?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Malcolm Lightbody"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Malcolm Lightbody</span></a> 
---
Since I don't want to install and manage a compatible version of Ruby for my _Jekyll_ version and all dependencies of my website I decided to use [Jekylls' Docker image](https://hub.docker.com/r/jekyll/jekyll/) to develop this page. For my build pipeline in Gitlab CI I wanted to make use of these already existing definitions.

I created a simple `docker-compose.yml` file that loads my source files as a volume, like explained in the [documentation of Jekyll's Docker Image](https://github.com/envygeeks/jekyll-docker/blob/master/README.md#usage):

```yml
version: '3.7'

services:
  blog:
    image: jekyll/jekyll:3.8.5
    command: jekyll serve --watch --force_polling --verbose
    ports:
      - 4000:4000
    volumes:
      - ./blog:/srv/jekyll
```

For simple usage in development I defined `jekyll serve` as my default command so that I can use `docker-compose up` to be ready for development.

In Gitlab CI you can use the docker-engine directly which would allow to directly load the jekyll image but unfortunately [you can't define a volume yet](https://gitlab.com/gitlab-org/gitlab-runner/issues/3207). So their needed to be another solution.

I came up with a Docker-in-Docker setup. The idea is to start Docker within the Container that gets started by Gitlab CI and define the volume in it. Even better, we can make use of our already existing `docker-compose` file that already defines the Jekyll version and the path to our page.

So I came up with the following pipeline:

```yml
image: tmaier/docker-compose:18.09

services:
  - docker:dind

build-blog:
  script:
    - docker-compose -f docker-compose.yml run -e JEKYLL_ENV=production blog jekyll build
```

The image I'm using is well maintained and provides `docker-compose`. The [docker:dind](https://github.com/docker-library/docker/blob/157869f94ea90e2acb4d0f77045d99079ead821c/18.02/dind/Dockerfile) service starts the docker daemon as its entrypoint.

With that up and running we can simply use our `docker-compose.yml` file and run it with the `jekyll build` command. The `-e JEKYLL_ENV=production` sets the `JEKYLL_ENV` environment that is used by many plugins to do optimizations.

Now we can proceed with the resulting artifact and do whatever we need to do. In my case I have another stage that deploys the page on my webserver.

#### Update

[Check out the source code](https://gitlab.com/marcus.held/code-held/blob/52db9bbda369bdc25437cd77ebf2cf1e20cc4780/.gitlab-ci.yml) in the repository of this page.
