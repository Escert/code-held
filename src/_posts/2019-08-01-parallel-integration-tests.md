---
layout: post
title: Parallel Integration Tests With Random Ports On Jenkins
image: assets/images/posts/2019/08/parallel-tests.jpg
image-alt: Straight Railways
tags: [Jenkins, Continuous Integration, Gradle, Docker, Java, Kotlin]
highlight: true
call-to-action: Did this solution help you? Tell me! 
credits: Post Image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@titi_wanderer?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Thong Vo"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Thong Vo</span></a>
---
A common problem when doing end-to-end tests is colliding ports on you buildmachine with parallel execution. With the technology stack of Docker, Jenkins and Gradle I'll demonstrate one solution I use in my current project to start the backend with a random port and use it in the test execution afterwards.

Our situation is the following: You want to execute a test which starts your backend application and fires against it from the outside. That way you can make sure that your whole infrastructure of the backend is working, including the servlet container for example. But the problem lies in the small detail that such tests usually take a while to execute and therefore lengthen your job runtime while blocking a single port over the time of execution. So the simplest solution by just starting the backend on a fixed port does not scale well for larger teams where a bunch of jobs are executed all the time. Therefore we'd think about starting the backend with randomly assigned ports. The graphic below demonstrates what's happening on a single executor on Jenkins.

<div class="center-align">
{% graphviz %}
digraph {
    label = "Jenkins"
    rankdir = LR
    splines = ortho
        
    subgraph cluster_job_2 {
        label = "Job 2"
        
        container_2 [shape=plaintext label=<
                       <table border="1" cellborder="0" cellspacing="1" valign="middle">
                         <tr><td align="center">Backend Container</td></tr>
                         <tr><td align="center"><font point-size="10">Port 8080</font> <font point-size="10" color="red">conflict</font></td></tr>
                       </table>>]
        task_start_backend_2 [label="Task Test"]
        test_2 [label="Integration Test"]
        
        task_start_backend_2 -> container_2 [label="starts"]
        container_2 -> test_2 [label="when ready"]
        test_2 -> container_2 [label="uses"]
    }
    
    subgraph cluster_job_1 {
        label = "Job 1"
        
        container_1 [shape=plaintext label=<
                       <table border="1" cellborder="0" cellspacing="1" valign="middle">
                         <tr><td align="center">Backend Container</td></tr>
                         <tr><td align="center"><font point-size="10">Port 8080</font></td></tr>
                       </table>>]
        task_start_backend [label="Task Test"]
        test_1 [label="Integration Test"]
        
        task_start_backend -> container_1 [label="starts"]
        container_1 -> test_1 [label="when ready"]
        test_1 -> container_1 [label="uses"]
    }
    
}
{% endgraphviz %}
</div>

> note "Source Code"
> The whole example is available on [GitHub](https://github.com/mld-ger/random-port-example)

Our example backend is a very simple service written with Spring Boot and Kotlin which has one endpoint we want to test against:

`backend/src/main/kotlin/de/held/randomport/backend/ExampleController.kt:`
{% highlight kotlin %}
@RestController
class ExampleController {

	@GetMapping("example")
	fun exampleEndpoint() = "foo"

}
{% endhighlight %}

The test should happen through another application, because we want to test against the actual `jar` of our backend. So we create another application, called [integration-test](https://github.com/mld-ger/random-port-example/tree/master/integration-test) that only consists of a single unit test written with JUnit in Java:

`integration-test/src/test/java/de/held/randomport/integrationtest/BackendIntegrationTest.java:`
{% highlight java %}
@Test
public void TestBackendExampleEndpoint() throws Exception {
    HttpClient httpClient = HttpClient.newHttpClient();
    HttpRequest request = HttpRequest.newBuilder()
            .GET()
            .uri(new URI(backendUrl + "/example"))
            .build();
    HttpResponse<String> response = httpClient.send(request, BodyHandlers.ofString());

    Assertions.assertEquals("foo", response.body());
}
{% endhighlight %}

As you can see the test is quite simple and performs a `GET` request against the `/example` endpoint we defined earlier to check if the expected body is received. The hard problem to solve lies in the {% ihighlight java %}backendUrl{% endihighlight %} variable. We need to point the test to the correct address with the correct port the server started on. For local testing this is easy, it's just the configured port of the backend application, but as soon as we want to execute the test on Jenkins or any other buildmachine we want to be independent of the port to avoid conflicts with other builds.

So first we need the functionality to start the backend with a random port. For that purpose we use `docker-compose`. The configuration is simple again:

`docker-compose.yml:`
{% highlight yml %}
version: '3'
services:
  backend:
    build:
      dockerfile: backend/Dockerfile
      context: .
    ports:
      - "8080"
{% endhighlight %}

By configuring the service with `ports` `"8080"` `docker-compose` will take an available port for us to expose and maps it on the port `8080` inside of the container, which is the port we configured for our backend application.

Starting our setup with `docker-compose up` shows us that we assign a random port.

{% asset posts/2019/08/dockerps.png class="responsive-img materialboxed" style="width: 100%" data-caption="docker ps command after starting the backend shows a random port" alt="docker ps command after starting the backend shows a random port" %}

As a next step we need to pass this port to our `integration-test`. To do so we use the [docker-compose](https://github.com/avast/gradle-docker-compose-plugin) plugin from avast. With it we have the possibility to start our container with gradle and receive information about it afterwards which we can pass through system properties to the integration-test. 

`integration-test/build.gradle`:
{% highlight groovy %}
dockerCompose.isRequiredBy(test)

dockerCompose {
	useComposeFiles = ['../docker-compose.yml']
	captureContainersOutput = true
}

test.doFirst {
    // exposes "${serviceName}_HOST" and "${serviceName}_TCP_${exposedPort}" environment variables
    // for example exposes "WEB_HOST" and "WEB_TCP_80" environment variables for service named `web` with exposed port `80`
	dockerCompose.exposeAsSystemProperties(test) 
}
{% endhighlight %}

Within our integration-test we can read the system properties and assign it to a field which is accessible in our test:

`integration-test/src/test/java/de/held/randomport/integrationtest/BackendIntegrationTest.java:`
{% highlight java %}
public class BackendIntegrationTest {

	private static String backendUrl;

	@BeforeAll
	public static void ReadBackendUrl() {
		String backendHost = System.getProperty("backend.host");
		String backendPort = System.getProperty("backend.tcp.8080");
		backendUrl = "http://" + backendHost + ":" + backendPort;
	}
	
	(...)
}	
{% endhighlight %}

And that is it already. When we execute `./gradlew integration-test:test` we start the docker container with a random port and the test picks it up. This can be easily executed on any build machine without interfering other builds.

> warning "Warning"
> When you want to run a setup like this I recommend configuring an additional cronjob that cleans up dangling containers from time to time. It can happen that for some random reason a container might not stop properly.
