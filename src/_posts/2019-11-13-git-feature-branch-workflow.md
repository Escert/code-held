---
layout: post
title: Automate Your Git Commits On Feature Branches
image: assets/images/posts/2019/11/git-feature-branch.jpg
image-alt: A GitHub USB stick in a laptop
tags: [Git, Best Practice]
highlight: false
call-to-action: What do you think? Get in touch with me and 
credits: Post image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@brina_blum?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Brina Blum"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Brina Blum</span></a>
---
At some point it hits everyone. Your precious work of several hours is vanished because of a hardware failure. For that the industry came up with several solutions like the idea of version control systems. But recently I was dumb enough to not do any commit for several hours - just to leave the history clean. Big mistake... 
But did you think about automating that? How about committing every couple of minutes automatically? In this post I'll explain this - on the first sight - weird workflow.

So, the idea is, to have a script that commits every couple of minutes as long as you're working on a project. As soon as you are finished with your feature branch you use *interactive rebasing* to get rid of all the temporary commits.

The following one-liner is the command we want to automate:

{% highlight bash %}
git add -A && git commit -m "WIP" && git push -u origin
{% endhighlight %}

Basically you add all files to the index, commit with a default message and push the new commit to origin with one go.

The next step is to write a script which executes this command regular when there are changes in the given repository. The simplest script that does that looks like so:

{% highlight bash %}
#!/usr/bin/env bash
while :
do
  sleep 300
  git add -A && git commit -m "WIP" && git push -u origin
done
{% endhighlight %}

But obviously you should optimize the script a bit. You should get some better feedback what the script is currently doing and you should add a safety net that the script aborts as soon as you switch the branch. So a nicer solution is the following:

{% highlight bash %}
#!/usr/bin/env bash

function getCurrentBranch() {
    git rev-parse --abbrev-ref HEAD
}

COMMIT_INTERVAL=300 # This is the interval in seconds between our commits
CURRENT_BRANCH=$(getCurrentBranch) # We store the branch on which we were when we executed the script

echo "Starting feature branch workflow on $CURRENT_BRANCH will commit every $COMMIT_INTERVAL seconds"
while :
do
  sleep $COMMIT_INTERVAL
  # When we switch the branch we detect that before we continue with the commiting and pushing.
  if [ $CURRENT_BRANCH != "$(getCurrentBranch)" ]; then
    echo "You checked out another branch then you started with. For safety reasons I'll terminate"
    exit 0
  fi
  git add -A && git commit -m "WIP" && git push -u origin
done
{% endhighlight %}

When you're finished with the feature branch you probably want to have a nicer history without all the WIP commits. For that you can use *interactive rebasing*. This git function allows you to rewrite the history of a branch. To start the process you type: {% ihighlight bash %}git rebase -i master{% endihighlight %}

> info "Info"
> When you branched of from another branch then *master* you need to exchange that in the rebase command of course.

The command will open your configured editor with a file like the following:

```text
pick 1778493 WIP
pick ce1ee68 WIP
pick 282cdee WIP
```

In order to combine all the commits you exchange the all `pick` keywords but the first with `squash` or `s`:

```text
pick 1778493 WIP
s ce1ee68 WIP
s 282cdee WIP
```

When we save and exit the file we get to edit the commit message for the squashed commits. 

And that's it. We can now run the above script as soon as we start to work on a feature branch and we're sure that the changes will get pushed regular on remote.

#### Update 2019-11-14

After playing a bit more with the workflow I did a couple of improvements to the script which result in a nicer output.

{% highlight bash %}
#!/usr/bin/env bash
set -euo pipefail # Bash strict mode - see http://redsymbol.net/articles/unofficial-bash-strict-mode/

function getCurrentBranch() {
    git rev-parse --abbrev-ref HEAD
}

function currentTime() {
  echo "[$(date +"%T")]"
}

COMMIT_INTERVAL=5 # This is the interval in seconds between our commits
CURRENT_BRANCH=$(getCurrentBranch) # We store the branch on which we were when we executed the script

printf "%s Starting feature branch workflow on $CURRENT_BRANCH will commit every $COMMIT_INTERVAL seconds" "$(currentTime)"
while :
do
  sleep $COMMIT_INTERVAL
  # When we switch the branch we detect that before we continue with the commiting and pushing.
  if [ $CURRENT_BRANCH != "$(getCurrentBranch)" ]; then
    printf "%s You checked out another branch then you started with. For safety reasons I'll terminate" "$(currentTime)"
    exit 0
  fi
  if [ ! -z "$(git status --porcelain)" ]; then
    printf "\n\n%s About to commit: \n" "$(currentTime)"
    git status -s
    git add -A &>/dev/null && git commit -m "WIP" &>/dev/null && git push -u origin &>/dev/null
    printf "%s Success" "$(currentTime)"
  fi
done
{% endhighlight %}

These improvements result in an output like that:

{% asset posts/2019/11/git-feature-branch-example.png class="responsive-img materialboxed" width="500" data-caption="Example output of the workflow script" alt="Example output of the workflow script" %}

You might also notice that I changed the commit interval to 5 seconds because I noticed that I prefer to persist changes as soon as possible. I actually adapted my workflows to work with many commits. [For example I avoid triggering CI builds when "WIP" is part of the commit message](https://gitlab.com/marcus.held/code-held/blob/c36e5d03d95daf236193ac4fb64292e06b8d8161/.gitlab-ci.yml#L14). Furthermore I use the _[git-squash]({% post_url 2019-08-08-git-squash %})_ alias to squash all commits when I finished working.

In some cases you'd like to mark specific states of your history. For that I create an empty commit with {% ihighlight bash %}git commit --allow-empty -m "Some message"{% endihighlight %}.
