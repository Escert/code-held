---
layout: post
title: How I (also) use JMeter for Integration Tests
image: assets/images/posts/2019/09/jmeter-title.jpg
image-alt: A Volt Meter
tags: [Jenkins, Docker, JMeter]
highlight: false
call-to-action: I hope this small post inspired you to give it a try as well. Tell me about it.
credits: Post image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@thkelley?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Thomas Kelley"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Thomas Kelley</span></a>
---
In my current project I focus on the development of a highly available, scalable system that can handle millions of players worldwide. In order to test and ensure that this is actually the case we need to do regular performance and integration tests. While - for a long time - I explicitly separated both, I choose a new direction in this project by writing the JMeter performance test in a way that it can be used as an integration test as well.

This approach has two major benefits. First, you only have to maintain one test in order to fulfill both quality aspects and second you - more likely - end up with a fully fleshed integration and performance test.

But it also means, that your performance test must be executable and in best case executable in parallel on your buildmachine. I do the following: 

{% asset posts/2019/09/JMeter-integration-test.svg %}

As you can see I use docker to execute the test and the backend. This way I can ensure the correct order and work in my own network which enables the test to be run in parallel.

The jmeter client is inspired by the [docker-jmeter-client](https://github.com/hhcordero/docker-jmeter-client) and uses [wait-for](https://github.com/Eficode/wait-for) to detect when the backend is ready to process requests. 

As a result from the jmeter client you get a file which you can analyze with Jenkins [Performance Plugin](https://wiki.jenkins.io/display/JENKINS/Performance+Plugin). My test plan is executed with a single thread by default and the performance plugin checks if no error occurred. Otherwise the build fails.

The big benefit is that I can simply increase the number of threads in JMeter now to turn the same testplan into a performance test.
