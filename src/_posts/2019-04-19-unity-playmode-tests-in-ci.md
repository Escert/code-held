---
layout: post
title: Randomly Failing Playmode Tests in Unity
image: assets/images/posts/2019/04/unity.png
image-alt: Unity Logo
tags: [Unity]
call-to-action: Did this article help you? I'd love to hear from you.
---
For the last year my current project developed several playmode tests in Unity. These tests get executed by our Jenkins as part of our continues integration strategy. But since we started using them we experienced strange randomly failing tests because they reached their timeout.

Today we finally found the issue! Until now (version 2018.2), Unity (on MacOS? We don't build on Windows machines) slows down when no output device is detected and the user is not logged in.

So the (very unsatisfying) solution is attaching a monitor and logging in the the user on our buildmachines.
