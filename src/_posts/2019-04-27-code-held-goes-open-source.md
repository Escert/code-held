---
layout: post
title: Code-Held Goes Open Source
image: assets/images/posts/2019/04/open.jpg
image-alt: We are open sign
tags: [open source]
call-to-action: Did this article help you? I'd love to hear from you.
credits: Post image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@alvaroserrano?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Álvaro Serrano"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Álvaro Serrano</span></a>
---

Today I have an awesome Message. Since I also post about technologies I use on this page I decided to put the whole page [open source on gitlab](https://gitlab.com/marcus.held/code-held). Feel free to discover how I developed this blog and which technologies I use.

I had the project in a private repository before but after much considerations I decided to not transform it directly into public space, because I used to have my nginx config in there as well and at some point unmasked ssh keys (I exchanged them of course but you never know...). So I put the latest state of the page into a new repository and took care that the SSH key that is used for publishing the website is properly masked in the pipeline.

The old posts also got updated to have direct links into the repository to the place where the explained topic is used.

You can also contribute if you like and create merge-requests or issues. If you want to do a guest post feel free to get in contact with me.
