---
layout: post
title: How To Squash All Commits Of Your Feature Branch
image: assets/images/posts/2019/08/git-squash-post-image.jpg
image-alt: Boy squeezing a ball
tags: [Git]
highlight: false
call-to-action: Do you like this alias? Tell me your feedback
credits: Post Image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@rankandfiledostoevsky?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Vance Osterhout"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Vance Osterhout</span></a>
---
Usually you have a lot of "intermediate" commits while developing on a feature branch like `WIP`, `Review changes`, `Some cleanup`, `Fix jenkins`. These commits are neither atomic nor does it help to read them in the history. They purely serve the purpose to persist the current work, trigger another build on your buildmachine or doing some fixes you discovered while testing. So before I rebase my changes to master I'd like to squash all commits that i've done to a single one. Usually I use interactive rebase for this purpose but since I need to actively tell every commit that I want to squash it I found it tedious. Also its easier, most of the time, to rebase onto master since you only apply conflicting changes to the end-result of your work without the intermediate steps you had to take. So I came up with my own git alias that solves this for me. Introducing: {% ihighlight bash %}git squash <Commit/Branch> <Message>{% endihighlight %}

#### TL;DR;

{% highlight bash %}
git config --global alias.squash '!bash -c '"'"'usage="Usage: git squash <Commit/Branch> <Message>";baseBranchParam=$0;if [ -z ${baseBranchParam+x} ];then echo "No base branch specified";echo $usage;exit 1;fi;git --no-pager show $baseBranchParam&>/dev/null;if [ $? -ne 0 ];then echo "No valid git object specified.";echo $usage;exit 1;fi;commitMsg=$1;if [ -z "$commitMsg" ];then echo "No commit message specified for squash commit.";echo $usage;exit 1;fi;currentBranch=$(git rev-parse --abbrev-ref HEAD);baseCommit=$(git merge-base $currentBranch $baseBranchParam);printf "Squashing the following commits:\n\n";printf "$(git --no-pager log --format='\"'%H %an - %s'\"' $baseCommit..$currentBranch)\n\n";git reset --soft $baseCommit&>/dev/null;git commit -m "$commitMsg" 1>/dev/null;if [ $? -ne 0 ];then echo "Squashed into new commit \"$commitMsg\"";exit 0;fi;exit 1'"'"' $1 $2'
{% endhighlight %}

Usage:

{% highlight bash %}
git squash <Commit/Branch> <Message>
{% endhighlight %}

Where `<Commit/Branch>` is the parent from where you started your branch and `<Message>` is the commit message for your squashed commit.

#### Long Version

Let's first demonstrate what we want to achieve. Imagine we are in the following state in our development:

{% graphviz %}
digraph {
	rankdir="LR"
	bgcolor="transparent"
	node[width=0.15, height=0.15, shape=point]
	edge[weight=2, arrowhead=none]
	node[group=master]
	1 -> 2 -> 3 -> 4 -> 5
	5 -> master[style=invis]
	master[shape=none, width=1]
	node[group=branch, color="#ff9800"]
	2 -> 6 -> 7 
	7 -> feature[style=invis]
	feature[shape=none, width=1]
	{rank=same master feature}
}
{% endgraphviz %}

Usually I would simply `rebase` the feature branch onto master (`rebase` because in larger teams you want to avoid a [merge hell](http://www.tugberkugurlu.com/archive/resistance-against-london-tube-map-commit-history-a-k-a--git-merge-hell)), but when we do so we end up with the following:

{% graphviz %}
digraph {
	rankdir="LR"
	bgcolor="transparent"
	node[width=0.15, height=0.15, shape=point]
	edge[weight=2, arrowhead=none]
	node[group=master]
	1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7
	7 -> master[style=invis]
	7[color="#ff9800"]
	6[color="#ff9800"]
	master[shape=none, width=1]
}
{% endgraphviz %}

The two commits from our feature branch are not atomic so we now have "broken" commits on our master. This can make it harder to use tools like {% ihighlight bash %}git bisect{% endihighlight %} to figure out where bugs were introduced. Also this forces you to write more or less useful commit messages on your feature branch, even when you don't have a working version and just want to persist your current state before leaving into the weekend. So I prefer to squash all my commits before I rebase the feature onto master.

Unfortunately it's not possible to (easily) use {% ihighlight bash %}git rebase --interactive{% endihighlight %} and squash all commits. But thinking about what we want to achieve it's actually easy with other commands. We want to get all changes until the commit we branched of, reset to it and commit all changes in a single commit. So basically the following is sufficient:

{% highlight bash %}
git reset --soft <base commit>
git commit -m <new commit msg>
{% endhighlight %}

As you might noticed we also need to figure out the `<base commit>`. This can be done by using {% ihighlight bash %}git merge-base <current branch> <base branch>{% endihighlight %}. `git merge-base` will figure out the best common ancestor of two branches. But now we need to figure out the `<current branch>`. There are multiple ways to do that. In the most recent git version `git branch --show-current` does exactly what you expect, but since this is a rather new feature I decided to rely on an older approach which is {% ihighlight bash %}git rev-parse --abbrev-ref HEAD{% endihighlight %}. So we finally have our logic to do a squash with minimal input:

{% highlight bash %}
#!/bin/bash

baseBranchParam=$1
commitMsg=$2

currentBranch=$(git rev-parse --abbrev-ref HEAD)
baseCommit=$(git merge-base $currentBranch $baseBranchParam)

git reset --soft $baseCommit
git commit -m "$commitMsg"
{% endhighlight %}

Now we add a couple of safety checks and some nice output and we end up with a script like this:

{% highlight bash %}
#!/bin/bash

usage="Usage: git squash <Commit/Branch> <Message>"

baseBranchParam=$1

# Check if a base branch parameter is set
if [ -z ${baseBranchParam+x} ]; then 
	echo "No base branch specified"
	echo $usage
	exit 1
fi

# Check if the base branch parameter is an actual git object
git --no-pager show $baseBranchParam &> /dev/null

if [ $? -ne 0 ]; then 
	echo "No valid git object specified." 
	echo $usage
	exit 1 
fi 

commitMsg=$2

if [ -z "$commitMsg" ]; then
	echo "No commit message specified for squash commit."
	echo $usage
	exit 1
fi

currentBranch=$(git rev-parse --abbrev-ref HEAD)

baseCommit=$(git merge-base $currentBranch $baseBranchParam)

printf "Squashing the following commits:\n\n"
printf "$(git --no-pager log --format='%H %an - %s' $baseCommit..$currentBranch)\n\n"

git reset --soft $baseCommit &> /dev/null
git commit -m "$commitMsg" 1> /dev/null

if [ $? -ne 0 ]; then
	echo "Squashed into new commit \"$commitMsg\""
	exit 0
fi

exit 1
{% endhighlight %}

We are now able to use the script like {% ihighlight bash %}./git-squash master "Implement new feature"{% endihighlight %} and it will squash all commits of the branch we are currently working on and put it into a new commit with the message "Implement new feature".

The last piece missing now is that we also want to have it as a proper git alias. To do that we add the following entry into our `.gitconfig`:

{% highlight bash %}
[alias]
    squash = !bash -c 'bash /path/to/your/script/git-squash $1 $2'
{% endhighlight %}

Finally we are able to use {% ihighlight bash %}git squash master "Implement new feature"{% endihighlight %}. 

{% asset posts/2019/08/git-squash.png class="responsive-img materialboxed" width="700" data-caption="The output of the git squash command" alt="The output of the git squash command" %}

> info "Information"
> Of course you can also put the bash script directly into your git alias with some escape magic. So if you want to add it as a "one-liner" you can use the command in the TL;DR; in the beginning of this post.

#### Update 2019-08-14
I updated the script a bit to not redirect the error message when {% ihighlight bash %}git commit{% endihighlight %} fails. This is handy because you might have pre-commit hooks that cause it to fail and you want to see the actual error message.
