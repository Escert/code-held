---
layout: post
title: How To Load A Shared Library From A Subfolder In Jenkins
image: assets/images/posts/2020/01/jenkins-library-subfolder-loader.png
image-alt: Jenkins next to a kid
tags: [Jenkins]
highlight: false
call-to-action: Do you make use of this technique. Tell me!
credits: Post image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@caleb_woods?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Caleb Woods"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Caleb Woods</span></a>
---
I work with Jenkins Pipeline for three years now and one pain point is proper isolation of shared functionality between pipelines but even steps. In our repository we defined multiple pipelines and some are that large that we share functionality within it. Jenkins offers the possibility to create [shared libraries](https://jenkins.io/doc/book/pipeline/shared-libraries/) for that purpose. But unfortunately it's not possible to load it from the same repository. Since many of the changes in the pipeline are related to a change of the shared library it was tedious to match the branches and versions to be backward compatible. What we actually wanted is having the shared library in our main repository so the states of the pipeline is pinned to the state of the main repository. And we finally figured out how to do (hack) that.

> note "Source Code"
> You can find the [jenkins-library-subfolder-loader](https://github.com/mld-ger/jenkins-library-subfolder-loader) and a working [example](https://github.com/mld-ger/jenkins-library-subfolder-loader-example) on Github.

Currently we have the limitation that we can't define a subfolder from the main repository to use as a shared library ([There's a PR open for 2.5 years](https://github.com/jenkinsci/workflow-cps-global-lib-plugin/pull/37)). So after searching the internet for a while I stumbled upon [this creative idea](https://stackoverflow.com/a/47892734/9857590) (hack). The idea is simple. You initialize a new git repository in the subfolder where your shared library lies and give the path to the library step that loads it.

{% highlight bash %}
cd ./path/to/library && \ # We change to the library subfolder
(rm -rf .git || true) && \ # To make sure that we have a clean state we remove any previous created repository
git init && \ # Now initialize the "fake" repository
git add --all && \ # And add all files
git commit -m init # And commit it 
{% endhighlight %}

After setting up the repository we can use the {% ihighlight groovy %}library{% endihighlight %} step to load it from the repository we just created.

{% highlight groovy %}
library identifier: 'local-lib@master', // The identifier doesn't matter
				retriever: modernSCM([$class: 'GitSCMSource', remote: "/path/to/library"]), // Here we load the library from disk
				changelog: false // We don't want to see the changelog of the fake repository
{% endhighlight %}

And this is essentially it. In the last step we combine both and execute it on the `master` node.

> warning "Attention"
> You can only load a shared library on the master node of your Jenkins instance.

{% highlight groovy %}
node("master") {
    echo "Loading local shared library"
    checkout scm

    // Create new git repo inside jenkins subdirectory
    sh("""cd ./$libraryPath && \
            (rm -rf .git || true) && \
            git init && \
            git add --all && \
            git commit -m init
    """)
    def repoPath = sh(returnStdout: true, script: 'pwd').trim() + "/$libraryPath"

    library identifier: 'local-lib@master', 
            retriever: modernSCM([$class: 'GitSCMSource', remote: "$repoPath"]), 
            changelog: false

    deleteDir() // After loading the library we can clean the workspace
    echo "Done loading shared library"
}
{% endhighlight %}

And this code - that loads the local library - can be a [shared library](https://github.com/mld-ger/jenkins-library-subfolder-loader) itself so we don't need to repeat ourselves for different projects or pipelines.

I implemented a [simple example](https://github.com/mld-ger/jenkins-library-subfolder-loader-example) to demonstrate how to use the loader. The project just has a Jenkinsfile which loads the shared library in the `library` subfolder.

{% highlight bash %}
.
├── Jenkinsfile
└── library
    └── vars
        └── localTest.groovy
{% endhighlight %}

And this is the *Jenkinsfile* that loads and uses the shared library:

{% highlight groovy %}
@Library('subfolder-library@1.0') _
loadLocalLibrary scm, "library"

pipeline {
	agent any
	stages {
		stage("Test") {
			steps {
				localTest()
			}
		}
	}
}
{% endhighlight %}

To make this work you need to configure the `subfolder-library` shared library under `Manage Jenkins > Configure System > Global Pipeline Libraries`

{% asset posts/2020/01/shared-library.png class="responsive-img materialboxed" width="500" data-caption="You need to configure the jenkins-library-subfolder-loader  as a shared library." %}

> info "Info"
> Depending on your job type you can also configure the `jenkins-library-subfolder-loader` directly in the job configuration. This has the neat effect that it runs in the sandbox and can be modified when replaying a run.
