---
layout: post
title: Custom Authentication Filter with Spring Security
image: assets/images/posts/2019/05/lock.jpg
image-alt: Yellow Lock
tags: [Spring, Security, Java]
highlight: true
call-to-action: Did this article help you? I'd love to hear from you.
credits: Post photo by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@chrispanas?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from chris panas"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">chris panas</span></a>
---
After answering a question on [stackoverflow](https://stackoverflow.com/questions/55992894/the-auto-inject-of-principle-will-be-null-if-i-manually-set-authentication/55995653#55995653) about how to configure _Spring Security_ with your own authentication mechanism I'd like to go into more details in this post. I'll implement a simple use case where the actual authentication is done by reading the _username_ and _password_ from the header of a request.

> error "Important"
> The example method we implement in this post is **NOT** a secure way of implementing authentication. I just choose it for the sake of simplicity to demonstrate how to register your own authentication in Spring Security.

> note "Source Code"
> A working example can be found on [Github](https://github.com/mld-ger/authentication-demo)

Let's first have a look in a successful [test](https://github.com/mld-ger/authentication-demo/blob/master/src/test/java/de/held/authenticationdemo/AuthenticationIntegrationTest.java) to understand how we do our authentication:

{% highlight java %}
@Test
public void testAuthentication_validHeadersSet_status200() {
	// First we set headers that identifies us
	HttpHeaders headers = new HttpHeaders();
	headers.add("x-user", "Marcus Held");
	headers.add("x-password", "SuperSecret");
	
	// Then we do the request to the secured endpoint
	RequestEntity requestEntity = new RequestEntity(headers, HttpMethod.GET, URI.create("/secure"));
	ResponseEntity<String> response = testClient.exchange(requestEntity, String.class);

	// And this request should be processed by our server
	Assertions.assertThat(response.getStatusCode().value()).isEqualTo(200);
}
{% endhighlight %}

The flow we want to implement is the following:

{% seqdiag %}
seqdiag {
  client  ->  DemoController [label = "GET /secure"];
              DemoController  ->  AuthenticationFilter;
                                  AuthenticationFilter -> AuthenticationFilter [label = "Read Headers"];
                                  AuthenticationFilter -> InMemoryUserStore [label = "findByUsernameAndPassword"];
                                  AuthenticationFilter <- InMemoryUserStore [label = "Principal"];
                                  AuthenticationFilter -> SecurityContext [label = "setAuthentication"];
              DemoController  <-  AuthenticationFilter;
  client  <-  DemoController;
}
{% endseqdiag %}

When a request to a secured endpoint is received by our server the `AuthenticationFilter` should be processed and decide, based on the sent headers, if the associated user is authenticated to access the endpoint.
Our [DemoController](https://github.com/mld-ger/authentication-demo/blob/master/src/main/java/de/held/authenticationdemo/DemoController.java) doesn't do anything interesting and is simply for demonstration purpose:

{% highlight java %}
@GetMapping("secure")
@ResponseBody
public String securedEndpoint(Principal principal) {
    return principal.toString();
}
{% endhighlight %}

#### Spring Security Configuration

_Spring Security_ provides the tools to secure specific endpoints precisely. This is done by implementing a [WebSecurityConfigurerAdapter](https://github.com/mld-ger/authentication-demo/blob/master/src/main/java/de/held/authenticationdemo/SecurityConfig.java):

{% highlight java %}
@EnableWebSecurity
public class SecurityConfig {

	@RequiredArgsConstructor
	@Configuration
	@Slf4j
	public static class UserWebSecurity extends WebSecurityConfigurerAdapter {

		private final InMemoryUserStore userStore;

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			/*
			 We need to decide where in the filter chain our filter is tested.
			 For our example we still want to support the default session functionality, 
			 so we put our own filter after the session filter. That way the
			 authentication by session is done before our own filter.
			 */
			http.addFilterAfter(new CustomAuthenticationFilter(userStore), ConcurrentSessionFilter.class);

			http.requestMatchers()
					.antMatchers("/**");

			http
					.authorizeRequests()
					.anyRequest().authenticated();
		}

	}
}
{% endhighlight %}

Within this configuration we are able to add custom `Filter` that gets registered by _Spring Security_ in the Servlet Container. _Spring_ already registers a bunch of [security filters](https://docs.spring.io/spring-security/site/docs/5.2.x/reference/html5/#security-filter-chain) which are executed in a specific [order](https://docs.spring.io/spring-security/site/docs/5.2.x/reference/html5/#filter-ordering):

1. ChannelProcessingFilter, because it might need to redirect to a different protocol
1. SecurityContextPersistenceFilter, so a SecurityContext can be set up in the SecurityContextHolder at the beginning of a web request, and any changes to the SecurityContext can be copied to the HttpSession when the web request ends (ready for use with the next web request)
1. ConcurrentSessionFilter, because it uses the SecurityContextHolder functionality and needs to update the SessionRegistry to reflect ongoing requests from the principal
1.  Authentication processing mechanisms - UsernamePasswordAuthenticationFilter, CasAuthenticationFilter, BasicAuthenticationFilter etc - so that the SecurityContextHolder can be modified to contain a valid Authentication request token
1. The SecurityContextHolderAwareRequestFilter, if you are using it to install a Spring Security aware HttpServletRequestWrapper into your servlet container
1. The JaasApiIntegrationFilter, if a JaasAuthenticationToken is in the SecurityContextHolder this will process the FilterChain as the Subject in the JaasAuthenticationToken
1. RememberMeAuthenticationFilter, so that if no earlier authentication processing mechanism updated the SecurityContextHolder, and the request presents a cookie that enables remember-me services to take place, a suitable remembered Authentication object will be put there
1. AnonymousAuthenticationFilter, so that if no earlier authentication processing mechanism updated the SecurityContextHolder, an anonymous Authentication object will be put there
1. ExceptionTranslationFilter, to catch any Spring Security exceptions so that either an HTTP error response can be returned or an appropriate AuthenticationEntryPoint can be launched
1. FilterSecurityInterceptor, to protect web URIs and raise exceptions when access is denied

Within this chain we need to put our own `Filter` to a proper position. In this example we put it after the `ConcurrentSessionFilter`. That way we support session handling but if that's not successful we authenticate by our own mechanism.

#### Filter Implementation

Our [CustomAuthenticationFilter](https://github.com/mld-ger/authentication-demo/blob/master/src/main/java/de/held/authenticationdemo/CustomAuthenticationFilter.java) extends from `GenericFilterBean` which is registered as a bean automatically as soon as an implementation is found by _Spring Boot_. That bean gives us the possibility to execute code and our goal is to call {% ihighlight java %}SecurityContextHolder.getContext().setAuthentication(authentication){% endihighlight %} with a valid authentication. By doing so the request is authenticated. Keep in mind that this authentication is only valid for this very request and therefore a valid authentication must be set for every other request as well. That's why we send the necessary header to authenticate with every request on a secured endpoint.

{% highlight java %}
@RequiredArgsConstructor
public class CustomAuthenticationFilter extends GenericFilterBean {

	private final InMemoryUserStore userStore;

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;

		/*
		For our example we simply read the user and password information from the header and check if against our
		internal user store.
		IMPORTANT: THIS IS NOT A SECURE WAY TO DO AUTHENTICATION AND JUST FOR DEMONSTRATION PURPOSE!
		 */
		// First read the custom headers
		String user = request.getHeader("x-user");
		String password = request.getHeader("x-password");

		// No we check if they are existent in our internal user store
		Optional<CustomPrincipal> optionalCustomPrincipal = userStore.findByUsernameAndPassword(user, password);

		// When they are present we authenticate the user in the SecurityContextHolder
		if (optionalCustomPrincipal.isPresent()) {
			CustomAuthentication authentication = new CustomAuthentication(optionalCustomPrincipal.get());
			SecurityContextHolder.getContext().setAuthentication(authentication);
		}

		// In either way we continue the filter chain to also apply filters that follow after our own.
		chain.doFilter(request, response);

	}
}
{% endhighlight %}

In our example we check if the user with the given credentials is present in our custom `InMemoryUserStore` and if so we set the authentication in the `SecurityContextHolder`.

And that's it. You now successfully authenticated the request and it will be processed by _Spring MVC_. For a full example don't forget to checkout the [example project on Github](https://github.com/mld-ger/authentication-demo).
