---
layout: post
title: Useful Git Aliases That Ease Your Life
image: assets/images/posts/2019/07/git-aliases.jpg
image-alt: Sourcetree on a computer screen
tags: [Git]
highlight: false
call-to-action: Do you have useful aliases? Share them with me!
credits: Post Image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@yancymin?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Yancy Min"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Yancy Min</span></a>
---
When you work with git you can define aliases to make your experience even more productive and elegant. In this short post I'd like to present aliases that I use frequently and how they work. And I'll start with my favorite: {% ihighlight java %}git recent-branches{% endihighlight %}.

#### Recent Branches

{% highlight bash %}
git recent-branches
{% endhighlight %}

This command gives you the following output:

{% asset posts/2019/07/git-recent-branches.png class="responsive-img materialboxed" width="750" data-caption="The result of the recent-branches alias." alt="The result of the recent-branches alias." %}

This command shows you all branches you recently worked on. That solves the usual problem of me forgetting the names of the branches I worked on. 

> info "Tip"
> When you want to switch to the last branch you worked on you can use {% ihighlight bash %}git checkout -{% endihighlight %}

The alias is the following:

{% highlight bash %}
recent-branches = for-each-ref --sort=-committerdate refs/heads/ --count=15 --format='%(HEAD) %(color:yellow)%(refname:short)%(color:reset) - %(contents:subject) - %(authorname) (%(color:green)%(committerdate:relative)%(color:reset))'
{% endhighlight %}

> You can either add the alias to your config by executing {% ihighlight bash %}git config --global alias.[alias name] [alias command]. Alternativaly you can edit the `.gitconfig` file in your home folder.{% endihighlight %}

#### Daily

{% highlight bash %}
git daily
{% endhighlight %}

This is a very useful alias. It'll display all commits from the last 24h. This is the command to execute right before your daily standup to remind you of what you worked on the last day.

This is the alias:

{% highlight bash %}
daily = log --since '1 day ago' --oneline --author [your e-mail]
{% endhighlight %}

#### Graph

{% highlight bash %}
git graph
{% endhighlight %}

{% asset posts/2019/07/git-graph.png class="responsive-img materialboxed" width="500" data-caption="The result of the graph alias." alt="The result of the graph alias." %}

The default {% ihighlight bash %}git log --graph{% endihighlight %} command is not very clear and therefore I configured an alias which gives a simple overview of all commits and branches.

In the end the alias is simply a sophisticated version {% ihighlight bash %}git log{% endihighlight %}:

{% highlight bash %}
graph = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative
{% endhighlight %}

#### Skip & Unskip

This is not one alias but actually three of them. Sometimes you have files in the directories of your repository that don't belong in there but it is also not appropriate to add them to the `.gitignore`. In this case you can tell git to ignore these files and don't put them into the worktree. By doing so you can't stage them by accident.

The aliases are the following three:
{% highlight bash %}
git skip [file] // Removes the files from the worktree until you "unskip" them again
git unskip [file] // Adds the files again, so you can stage them
git list-skipped // Lists all files that you skipped previously
{% endhighlight %}

The aliases for these are the following:

{% highlight bash %}
skip = update-index --skip-worktree
unskip = update-index --no-skip-worktree
list-skipped = ! git ls-files -v | grep ^S
{% endhighlight %}
