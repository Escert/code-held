---
layout: post
title: Transparent Gradient Effect on Background Image
image: assets/images/posts/2019/04/painting.jpg
image-alt: Man painting wall in cyan
tags: [CSS, HTML, Webdesign]
call-to-action: Did this article help you? I'd love to hear from you.
credits: Post image by <a style="background-color:black;color:white;text-decoration:none;padding:4px 6px;font-family:-apple-system, BlinkMacSystemFont, &quot;San Francisco&quot;, &quot;Helvetica Neue&quot;, Helvetica, Ubuntu, Roboto, Noto, &quot;Segoe UI&quot;, Arial, sans-serif;font-size:12px;font-weight:bold;line-height:1.2;display:inline-block;border-radius:3px" href="https://unsplash.com/@dchuck?utm_medium=referral&amp;utm_campaign=photographer-credit&amp;utm_content=creditBadge" target="_blank" rel="noopener noreferrer" title="Download free do whatever you want high-resolution photos from Daniel Chekalov"><span style="display:inline-block;padding:2px 3px"><svg xmlns="http://www.w3.org/2000/svg" style="height:12px;width:auto;position:relative;vertical-align:middle;top:-2px;fill:white" viewBox="0 0 32 32"><title>unsplash-logo</title><path d="M10 9V0h12v9H10zm12 5h10v18H0V14h10v9h12v-9z"></path></svg></span><span style="display:inline-block;padding:2px 3px">Daniel Chekalov</span></a>
---
I built a new neat feature on my blog posts detail page that displays [the configured image]({% post_url 2019-04-21-jekyll-posts-image-with-assets-plugin %}) as a background image in the top of the rendered page. The trick to get the transparent fading effect via css is using a `linear-gradient` on top of the actual image.

It's important to state that the `linear-gradient` effect is applied to the whole size of the block. So The block must have the exact same size as the background image to produce this effect. Check out the snippet below to get an idea how to do that.

<p class="codepen" data-height="350" data-theme-id="0" data-default-tab="css,result" data-user="Plumpy" data-slug-hash="JVwBjJ" style="height: 265px; box-sizing: border-box; display: flex; align-items: center; justify-content: center; border: 2px solid black; margin: 1em 0; padding: 1em;" data-pen-title="Background Image Fade Effect">
  <span>See the Pen <a href="https://codepen.io/Plumpy/pen/JVwBjJ/">
  Background Image Fade Effect</a> by Marcus Held (<a href="https://codepen.io/Plumpy">@Plumpy</a>)
  on <a href="https://codepen.io">CodePen</a>.</span>
</p>
<script async src="https://static.codepen.io/assets/embed/ei.js"></script>

#### Update
Check out how I use this effect in the [source code](https://gitlab.com/marcus.held/code-held/blob/52db9bbda369bdc25437cd77ebf2cf1e20cc4780/src/_layouts/post.html#L5).
